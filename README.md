# shell-alert

1. go to `https://api.slack.com/apps/` ands create a Slack application

2. add a web-hook feature to the application

3. allow the application to posting to specific Slack channel

4. copy the Slack Webhook URL from the sample page to the script

5. run script as `tail -f "|Last update:" /var/log/syslog | ./shell-alert.sh`

That's it
