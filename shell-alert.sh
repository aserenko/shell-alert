#!/bin/bash

# !!! Put correct Slack URL here !!!
slackURL="https://hooks.slack.com/services/<FOO>/<BAR>/<TOKEN>"

# we send slack alert for last updates equal or bigger than the 'alertValue'
alertValue=120

while read LINE; do
  seconds=`expr "$LINE" : '.*Last update: \(.[0-9]*\)'`
  if [ $((seconds)) -ge $((alertValue)) ]; then
    alert_message="ALERT - $seconds seconds!!!: $LINE"
    messageData="{\"text\":\"$alert_message\"}"
    $(curl --silent --output /dev/null -X POST -H 'Content-type: application/json' --data "$messageData" "$slackURL")
  fi
done < /dev/stdin
